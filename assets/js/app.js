(function() {

	//DJ PAD;

	var DJPAD = {
		init : function() {
			this.setObjects();
			this.eventsBind();
		},
		//
		setObjects : function() {
			this.Pad = [];
			this.Sounds = [];
			for(var i = 1; i<10; i++){
				this.Pad[i] = document.querySelector('[data-key="'+i+'"]');
				this.Sounds[i] = document.querySelector('[data-sound="'+i+'"]');
			}
		},
		//bind
		eventsBind : function() {
			document.onkeydown 	= this.bindKeyDown.bind(this);
			document.onkeyup	= this.bindKeyUp.bind(this);
		},
		bindKeyDown : function(e) {
			var key = this.getKey(e);
			if(this.Pad[key]!= undefined) {
				this.Pad[key].classList.add('clicked');
				this.Sounds[key].currentTime  = 0;
				if(key != 5)
					this.Sounds[key].volume = 0.7
				this.Sounds[key].play();
			}
		},
		bindKeyUp : function(e) {
			var key = this.getKey(e);
			if(this.Pad[key]!= undefined) {
				this.Pad[key].classList.remove('clicked');
			}
		},
		//
		getKey : function(e) {
			return e.key || e.char;
		}
	};

	DJPAD.init();
	window.onload = function(){
		document.querySelector('.loading').classList.add('loaded')
	}
})();